import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoe } from "./dataShoe";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";
// import ItemShoe from "./ItemShoe";

export default class ShoeShop extends Component {
  state = {
    shoeArr: [],
    detail: dataShoe[0],
    cart: [],
  };
  handleShowDetailShoe = (shoe) => {
    this.setState({ detail: shoe });
  };
  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id === shoe.id;
    });
    if (index === -1) {
      let cartItem = { ...shoe, number: 1 };
      cloneCart.push(cartItem);
    } else {
      cloneCart[index].number++;
    }
    this.setState({ cart: cloneCart });
  };
  render() {
    return (
      <div className="container py-5">
        <Cart cart={this.state.cart} />
        <ListShoe
          handleAddToCart={this.handleAddToCart}
          handleShowDetailShoe={this.handleShowDetailShoe}
          shoeArr={this.state.shoeArr}
        />
        <DetailShoe detail={this.state.detail} />
      </div>
    );
  }
}
