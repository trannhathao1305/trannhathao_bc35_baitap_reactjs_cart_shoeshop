import React, { Component } from "react";
import { connect } from "react-redux";
import ItemShoe from "./ItemShoe";

class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.ListShoe.map((item, index) => {
      return (
        <ItemShoe
          handleClickShowDetail={this.props.handleShowDetailShoe}
          data={item}
          key={index}
          handleBuyShoe={this.props.handleAddToCart}
        />
      );
    });
  };
  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}

let mapStateToProps = (state) => {
  return {
    ListShoe: state.shoeReducer.shoeArr,
  };
};

export default connect(mapStateToProps)(ListShoe);
