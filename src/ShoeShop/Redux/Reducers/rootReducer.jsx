import { combineReducers } from "redux";
import { shoeReducer } from "../Reducers/shoeShopReducer";
export const rootReducer_ShoeShop = combineReducers({ shoeReducer });
